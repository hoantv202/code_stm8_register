#include "iostm8s003k3.h"
#include <stdint.h>
#include <intrinsics.h> // thu vien ngat


volatile unsigned long ms_count =0;
volatile unsigned long count =0;
volatile int bientam =0;
volatile unsigned int data_ADC16 =0;
volatile uint16_t PWM_duty =0;
volatile uint32_t countstatus =0x00;
volatile uint8_t out =0;
// dinh nghia
#define RTC_Hours_24 24
#define RTC_Minus_12 12

typedef struct 
{
  uint32_t RTC_Hours;
  uint32_t RTC_Minus;
}RTC_InitTypeDef;

RTC_InitTypeDef sTime;
typedef enum{
  ERROR = 0,
  SUCCESS = !ERROR
}ErrorStatsus;

void delay_ms(unsigned long ms)
{
  while(ms--);
}

ErrorStatsus RTC_Deinit(void)
{
  ErrorStatsus status = ERROR;
  do
  {
    countstatus ++;
    delay_ms(10000);
  }
  while(countstatus<100);
  if(countstatus >= 100)
  {
    status = ERROR;
  }
  else
  {
    status = SUCCESS;
  }
  return status;
}

void Check_Init(void)
{
  if(RTC_Deinit()==ERROR)
  {
    out =1;
  }
  if(RTC_Deinit()==SUCCESS)
  {
    out =2;
  }
}

void RTC_Init_Out(RTC_InitTypeDef *RTC_InitStruct)
{
  RTC_InitStruct->RTC_Hours = RTC_Hours_24;
  RTC_InitStruct ->RTC_Minus= RTC_Minus_12;
}

void Init_config(void)
{
  RTC_InitTypeDef a;
  a.RTC_Hours =1;
  a.RTC_Minus =30;
  RTC_Init_Out(&a);
}

void HSI_16MHz_Config()
{
  CLK_CKDIVR_HSIDIV = 0;
}

void IO_Config()
{
  PD_ODR_ODR0 = 1;
  PD_DDR_DDR0 =1;
  PD_CR1_C10 =1;
  PD_CR2_C20 =1;
  

  PD_DDR_DDR4 =1;
  PD_CR1_C14 =1;
  PD_CR2_C24 =1;
  
  PB_DDR_DDR7=0;
  PB_CR1_C17 =1;
  PB_CR2_C27 =1; // su dung ngat

}
void TIM2_Config()
{
  TIM2_PSCR = 0;
  TIM2_CR1_ARPE |= 1;

  TIM2_IER_UIE |= 1;
  TIM2_EGR_UG |= 1;
  TIM2_CNTRH  =0;
  TIM2_CNTRL =0;
  TIM2_ARRH = 0x3E ;
  TIM2_ARRL = 0x80 ;
  //TIM2_CCR1H = 0x3A;
 // TIM2_CCR1L = 0x98;
  TIM2_CCER1_CC1P = 1;
  TIM2_CCER1_CC1E =1;
  TIM2_CCMR1 =0x60;
  TIM2_SR1 |= (1<<0); // Xoa co ngat
  TIM2_CR1_CEN |= 1;
}

void TIM2_SetCompere(uint16_t duty_PWM)
{
 
 TIM2_CCR1H = (uint8_t)(duty_PWM >>8);
 TIM2_CCR1L = (uint8_t)(duty_PWM);

}
void TIM4_Config()
{
 // TIM4_PSCR = 0;
  TIM4_PSCR = 0x07;
  TIM4_ARR = 124;
  TIM4_CNTR = 0;
  TIM4_IER |= (1<<0); // Update Interrup enable
  TIM4_SR  |= (1<<0); // Clear flag interrup
  TIM4_CR1 = 0x81; // Counter enable
  //TIM4_EGR |= (1<<0);
  //__enable_interrupt();

}

void ADC_Singel_Mode(void)
{
  ADC_CSR_CH = 0; //AIN0
  ADC_CR1 |=(1<<1);
  ADC_CR1 |=(1<<0);
  ADC_CR2 |=(1<<3);
}
// ham tham so truyen vao la khong co ,tra ve la mot so nguyen kieu int
uint16_t Read_ADC(void)
{
   uint16_t data_ADC =0;
   ADC_CR1 |=(1<<0);
   // cho khi chuyen doi hoan thanh 
   while(ADC_CSR_EOC == 0U);
   data_ADC = ADC_DRL;
   data_ADC |= ADC_DRH << 8;
   return data_ADC;
}
void main()
{
  HSI_16MHz_Config();
 // RTC_Deinit();
  Init_config();
  Check_Init();
  asm("sim");
  IO_Config();
  TIM2_Config();
  //TIM4_Config();
 //ADC_Singel_Mode(); 
 // asm("rim");
    while(1)
      {
 
      //  PD_ODR_ODR0 = 1;
      //  delay_ms(100000);
       //  PD_ODR_ODR0 = 0;
      //  delay_ms(100000);

      for(PWM_duty = 0; PWM_duty <16000; PWM_duty +=10)
      {
        TIM2_SetCompere(PWM_duty);
        delay_ms(1000);
      }
      for(PWM_duty = 16000; PWM_duty >0; PWM_duty -=10)
      {
        TIM2_SetCompere(PWM_duty);
        delay_ms(1000);
      }

     // data_ADC16 = Read_ADC();
      if(bientam == 1)
      {
        delay_ms(400000);
        count ++;
        bientam =0;
      }

      }
  
  }

  //#pragma vector = TIM4_OVR_UIF_vector
  //__interrupt void ISR_TIM4(){

  #pragma vector = 0x06
  __interrupt void EXT0_PortB(void)
  {
    if(PB_IDR_IDR7 ==0 )
    {
    //  while(PB_IDR_IDR7 ==0);
       bientam = 1;
    }
     
  }
  #pragma vector =  0x19
  __interrupt void TIM4_OVR_UIF(void)
  {
   // if( (TIM4_SR & 0x01U)!= 0) && ()
   //TIM4_SR &= ~(1<<0) ;
  
   ms_count++;
   if(ms_count > 60000)
     {
        PD_ODR_ODR0 = !PD_ODR_ODR0;
        ms_count =0;
     }
  TIM4_SR =0;
  }